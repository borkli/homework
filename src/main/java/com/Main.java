package com;

import com.fintech.student.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);

        List<Student> students = createdStudents();
        //Суммарный возраст для имени
        System.out.println(getSumAgeByName("Иван", students));
        //Set имён
        getSetName(students).forEach(System.out::println);
        //Есть ли студент, возраст, которого больше опред. числа
        System.out.println(checkAgeStudents(19, students));
        //Map key - id, value - name
        getMapIdStudents(students).forEach((k, v) -> System.out.println(k + " " + v));
        //Map key - age, value - список студентов
        getMapAgeStudents(students).forEach((k, v) -> System.out.println(k + " " + v));
    }

    private static int getSumAgeByName(String name, List<Student> students) {
        return students.stream()
            .filter(s -> s.getName().equals(name))
            .mapToInt(Student::getAge).sum();
    }

    private static Set<String> getSetName(List<Student> students) {
        return students.stream()
            .map(Student::getName)
            .collect(Collectors.toSet());
    }

    private static boolean checkAgeStudents(int age, List<Student> students) {
        return students.stream().anyMatch(s -> s.getAge() > age);
    }

    private static Map<Integer, String> getMapIdStudents(List<Student> students) {
        return students.stream().collect(
            Collectors.toMap(Student::getId, Student::getName)
        );
    }

    private static Map<Integer, List<Student>> getMapAgeStudents(List<Student> students) {
        return students.stream().collect(
            Collectors.groupingBy(
                Student::getAge,
                Collectors.mapping(Function.identity(),
                    Collectors.toList()
                )
            )
        );
    }

    private static List<Student> createdStudents() {
        return Arrays.asList(
            new Student("Иван", 18),
            new Student("Мария", 18),
            new Student("Илья", 19),
            new Student("Георгий", 20),
            new Student("Марина", 19),
            new Student("Наталья", 35),
            new Student("Андрей", 24),
            new Student("Александр", 24)
        );
    }
}
