package com.fintech.student;

import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class StudentUtils {

    @CountStudents
    public List<Student> getBusyStudents(List<Student> students) {
        int hourNow = LocalDateTime.now().getHour();
        List<Student> result = new ArrayList<>();
        students.forEach(s -> {
            if (s.getTimeFrom() <= hourNow && hourNow <= s.getTimeTo()) {
                result.add(s);
            }
        });
        log.info("getBusyStudents " + LocalDateTime.now());
        return result;
    }

    public List<Student> csvParser(String path) {
        try {
            return new CsvToBeanBuilder<Student>(new FileReader(path))
                .withType(Student.class)
                .build().parse();
        } catch (Exception ex) {
            log.error("csvParser " + ex.getMessage() + LocalDateTime.now());
            ex.printStackTrace();
        }
        return null;
    }
}
