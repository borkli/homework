package com.fintech.student;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@NoArgsConstructor
public class Student {

    private static AtomicInteger count = new AtomicInteger(0);

    private int id = count.incrementAndGet();

    @CsvBindByName(column = "name")
    private String name;

    @CsvBindByName(column = "age")
    private int age;

    @CsvBindByName(column = "time_from")
    private int timeFrom;

    @CsvBindByName(column = "time_to")
    private int timeTo;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
