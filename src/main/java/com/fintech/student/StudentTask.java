package com.fintech.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.HOURS;

@Component
public class StudentTask {

    private final String PATH = "src/main/resources/students.csv";

    @Autowired
    private StudentUtils studentUtils;
    private List<Student> studentsPars;

    private final ScheduledExecutorService scheduler =
        Executors.newScheduledThreadPool(1);

    @EventListener(ContextRefreshedEvent.class)
    public void busyTask() {
        studentsPars = studentUtils.csvParser(PATH);
        final ScheduledFuture<?> studentBusy =
            scheduler.scheduleAtFixedRate(() ->
                studentUtils.getBusyStudents(studentsPars), 0, 1, HOURS);
    }
}
