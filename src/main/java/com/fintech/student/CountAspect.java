package com.fintech.student;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class CountAspect {

    private final String PATH = "src/main/resources/countStudents.txt";
    private static Map<String, Integer> data = new HashMap<>();

    @Around("@annotation(CountStudents)")
    private void countStudents(ProceedingJoinPoint point) throws Throwable {
        Object value = point.proceed();
        if (value.getClass() == ArrayList.class) {
            List<Student> students = (List<Student>) value;
            students.forEach(s -> data.merge(s.getName(), 1, Integer::sum));
        }

        FileWriter writer = new FileWriter(PATH);
        writer.write(LocalDateTime.now() + "\n");
        data.forEach((k, v) -> {
            try {
                writer.write(k + " - " + v + "\n");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        writer.close();
    }
}
